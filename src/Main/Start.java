/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import RunExtractErrors.ExtractError;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.simple.parser.ParseException;
import processing.DirectoryConfigs;
import processing.Logging;

/**
 *
 * @author Maryam khanzadi
 */
public class Start {

    public DirectoryConfigs confs;

    public static void main(String[] args) throws URISyntaxException, InterruptedException, JSONException, ParseException {
        try {
            Logging logs = new Logging();
            System.out.println(logs.LoggingStringDate() + " Start");//برگرداندن تاریخ شروع چک کردن فابل لاگ
            Start st = new Start();

            if (args == null || args.length == 0) {

                st.confs = new DirectoryConfigs(DirectoryConfigs.getCurrentPath() + "configs.JSON");//خواندن اطلاعات داخل کانفیک
            } else {
                st.confs = new DirectoryConfigs(args[0]);
            }
            ExtractError ExtractErr = new ExtractError(st.confs);
            ExtractErr.RunExtractTimer();
            System.out.println(logs.LoggingStringDate() + " System is sleeping (Time = 15 minutes)...");

            Thread.sleep(15 * 60 * 1000);

        } catch (InterruptedException ex) {
            Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
