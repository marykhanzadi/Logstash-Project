package processing;

import Main.Start;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author h.fakour
 */
public class NewsMeta {

    public String Recipients;
    public String name_index;
    public List<String> logs_path = new ArrayList();
    public ConcurrentHashMap<String, String> ServerUrls = new ConcurrentHashMap<>();
    public ConcurrentHashMap<String, Integer> LastLineRead = new ConcurrentHashMap<>();

    public NewsMeta(String Recipients, String name_index, List logs_path, String ServerUrls, List path, String current_path)//, int view_count, String[] keys)
    {
        this.Recipients = Recipients;
        this.name_index = name_index;
        this.ServerUrls.put(ServerUrls, name_index);
        for (int i = 0; i < path.size(); i++) {
            this.logs_path.add(current_path + path.get(i).toString());
            this.LastLineRead.put(current_path + path.get(i).toString(), 0);

        }

    }

    public List getLogs_path() {
        return logs_path;
    }

    public String getName_index() {
        return name_index;
    }

    public void setName_index(String name_index) {
        this.name_index = name_index;
    }

    public String getRecipients() {
        return Recipients;
    }

    public void setRecipients(String Recipients) {
        this.Recipients = Recipients;
    }

    public void setLastLineRead(ConcurrentHashMap<String, Integer> LastLineRead) {
        this.LastLineRead = LastLineRead;
    }

    public void setLogs_path(List<String> logs_path) {
        this.logs_path = logs_path;
    }

    public void setServerUrls(ConcurrentHashMap<String, String> ServerUrls) {
        this.ServerUrls = ServerUrls;
    }

    public ConcurrentHashMap<String, Integer> getLastLineRead() {
        return LastLineRead;
    }

}
