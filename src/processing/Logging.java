
package processing;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author h.fakour
 */
public class Logging {
    

    public String LoggingStringDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("'['yyyy-MM-dd HH:mm:ss.SSS']'");
        return sdf.format(new Date());
    }
    
}
