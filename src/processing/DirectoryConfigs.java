package processing;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Maryam khanzadi
 */
public class DirectoryConfigs {

    public String From;
    public String Pass;
    public String ReportPath;
    public Long duration_log;
    public Long duration_solr;

    public List<NewsMeta> crawler = new ArrayList();

    public DirectoryConfigs(String config_file_dir) throws JSONException, ParseException, URISyntaxException {
        String current_path = getCurrentPath();
//             GetConfigurations(config_file_dir);
        ReadConfig(config_file_dir, current_path);
//            this.Resources = current_path + configs.get(0);
//            // LogFiles.add(configs.get(1).split(","));
//            String[] logs = configs.get(1).split(",");
//            for (String log : logs) {
//                this.LogFiles.add(Resources + log);
//                this.LastLineRead.put(Resources + log, 0);
//            }
//            String[] serverurls = configs.get(2).split(",");
//            String[] cores = configs.get(3).split(",");
//            for (int i = 0; i < serverurls.length; i++) {
//                this.ServerUrls.put(serverurls[i], cores[i]);
//
//            }
//            this.From = configs.get(4);
//            this.Pass = configs.get(5);
//            this.Recipients = configs.get(6);
//            this.ReportPath = configs.get(7);
    }

//    private void  GetConfigurations(String ConfigFileDir) throws JSONException, ParseException {
//       ReadConfig(ConfigFileDir);
//        
//    }
    private void ReadConfig(String ConfigFileDir, String current_path) throws JSONException, ParseException {
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(new FileReader(ConfigFileDir));
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject meta = (JSONObject) jsonObject.get("meta");
            this.From = (String) meta.get("From");
            this.Pass = (String) meta.get("Pass");
            this.ReportPath = current_path + (String) meta.get("ReportPath");
            this.duration_log = (Long) meta.get("duration_log");
            this.duration_solr = (Long) meta.get("duration_solr");

//Path path = Paths.get(aFileName.replaceFirst("/", "").trim());
            JSONObject Crawler = (JSONObject) jsonObject.get("news_crawler");
            NewsMeta newmeta;
            String ServerUrls = (String) meta.get("solr_server_germany");
            String index = (String) Crawler.get("name_index");
            List logs_path;
            logs_path = (List) Crawler.get("logs_path");
            newmeta = new NewsMeta(
                    (String) Crawler.get("Recipients").toString().replace("[", "").replace("]", "").replace("\"", ""),
                    index,
                    (List) Crawler.get("logs_path"),
                    ServerUrls,
                    logs_path,
                    current_path
            );
            this.crawler.add(newmeta);
            Crawler = (JSONObject) jsonObject.get("twitter_crawler");
            ServerUrls = (String) meta.get("solr_server_germany");
            index = (String) Crawler.get("name_index");
            logs_path = (List) Crawler.get("logs_path");
            newmeta = new NewsMeta(
                   (String) Crawler.get("Recipients").toString().replace("[", "").replace("]", "").replace("\"", ""),
                    index,
                    (List) Crawler.get("logs_path"),
                    ServerUrls,
                    logs_path,
                    current_path
            );
            this.crawler.add(newmeta);
            // loop array
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }

//        File file = new File(ConfigFileDir);
//        List<String> confs = new ArrayList();
//        try {
//            Scanner sc = new Scanner(file);
//            while (sc.hasNext()) {
//                confs.add(Splitter(sc.nextLine()));
//            }
//        } catch (FileNotFoundException ex) {
//            System.err.println("The configuration file has problem.");
//        }
//        return (confs);
    }

    public static String getCurrentPath() throws URISyntaxException {
        URI path = DirectoryConfigs.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        String jar_path = path.getPath();
        return jar_path.replace("logstash.jar", "").replace("build/classes/", "");
    }

}
