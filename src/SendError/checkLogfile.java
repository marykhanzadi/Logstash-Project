/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SendError;

/**
 *
 * @author Maryam khanzadi
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import processing.DirectoryConfigs;
import processing.Logging;

public class checkLogfile {

    private static DirectoryConfigs confs;

    public checkLogfile(DirectoryConfigs configs) {
        confs = configs;
    }

    public Integer findBadLines(String aFileName, Integer LastLineRead, int indexcrawlerlist) throws IOException, Exception {
        Logging logs = new Logging();
        String ContentAttachFile = "";
        Pattern regexp = Pattern.compile(
                "\\[[\\d]+-[\\d]+-[\\d]+ [\\d]+:[\\d]+:[\\d]+.[\\d]+\\] \\[Error\\](.)+|"
                + "Error :(.)+|"
                + "\\[[\\d]+-[\\d]+-[\\d]+ [\\d]+:[\\d]+:[\\d]+.[\\d]+\\] \\[TimeOut\\] \\=\\> Link: http://localhost:5000/normalizer(.)+|"
                + "com.sun.(.)+|"
                + "javax.net(.)+"
        );
        Pattern regexpDatesleep = Pattern.compile("\\[[\\d]+-[\\d]+-[\\d]+ [\\d]+:[\\d]+:[\\d]+.[\\d]+\\] System is sleeping \\(Time = 15 m(.)+");
//--------------------------------------------------------------------------
        Matcher matcher = regexp.matcher("");
        Matcher matcherDatesleep = regexpDatesleep.matcher("");
        String line;
        try {

            Path path = Paths.get(aFileName.replaceFirst("/", "").trim());
            int n = LastLineRead;
            List<String> fileLines = Files.readAllLines(path);

            while (n < fileLines.size()) {
                line = fileLines.get(n);

                matcherDatesleep.reset(line);//cheak sleep system more than 15 minutes
                if (matcherDatesleep.find()) {
                    String datesleep = line.substring(1, 19);
                    Date dsleep = ConvetStringToDate(datesleep, 15, "yyyy-MM-dd HH:mm:ss");
                    int m = n + 1;
                    if (m < fileLines.size()) {
                        String linetemp = fileLines.get(m);
                        String datenextline = linetemp.substring(1, 19);//get date string 
                        Date dnextline = ConvetStringToDate(datenextline, 0, "yyyy-MM-dd HH:mm:ss");
                        if (dnextline.getHours() > dsleep.getHours() || (dnextline.getMinutes() - dsleep.getMinutes() >= 15)) {
                            String msg = line.trim() + "Sleep system more than 15 minutes: Sytem Start on  " + datenextline;
                            ContentAttachFile += msg + "\n";
                        }
                    }
                }
                matcher.reset(line); //reset the input
                if (matcher.find()) {
                    // String msg = "Line " + n + " is bad: " + line;
                    ContentAttachFile += line.trim() + "\n";
                }
                n += 1;
            }
            if (!ContentAttachFile.isEmpty()) {
                SendMessage sendmsssg = new SendMessage();
                String msssgage_text = "List Of Errors in log file in the last 15 minutes  \n" + ContentAttachFile;
                File Filepath = writing(msssgage_text);
                sendmsssg.sendDocUploadingAFile( Filepath,"Error Log File", "Check Log file =>" + path.getFileName());
                Filepath.delete();
//                MailerApp mailutils = new MailerApp();
//                mailutils.EmailSend(
//                        "List Of Errors in log file in the last 8 hours",
//                        "<br> Hi <br>  "
//                        + "List Of Errors in " + path.getFileName() + " file in the last 8 hours ,has been attached. <br> <br> Thanks <br> Mail sent by  Java code copied from: " + confs.From,
//                        ContentAttachFile, path.getFileName() + "_", confs, " Error in file log: " + aFileName,indexcrawlerlist);
            }
            System.out.println(logs.LoggingStringDate() + " Last Line read is: " + n + " From " + aFileName);
            return n;
        } catch (Exception ex) {
            System.err.println("Exception: Date Extraction => findBadLines " + ex);
        }
        return 0;
    }

    public File writing(String Message) {
        try {
            //Whatever the file path is.

            String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(
                    Calendar.getInstance().getTime());
            String file = timeLog + ".txt";
            File logFile = new File(confs.ReportPath + "Message.txt" + file);
            FileOutputStream is = new FileOutputStream(logFile);
            OutputStreamWriter osw = new OutputStreamWriter(is);
            Writer w = new BufferedWriter(osw);

            w.write(Message);

            return logFile;
        } catch (IOException e) {
            System.err.println("Problem writing to the file statsTest.txt");

        }
        return null;

    }

    public Date ConvetStringToDate(String date, Integer minute, String FirstFormat) {

        DateFormat formatter = new SimpleDateFormat(FirstFormat);//"yyyy-MM-dd'T'HH:mm:ss'Z'"   dd/MM/yyyy HH:mm:ss
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(formatter.parse(date.trim()));
            cal.add(Calendar.MINUTE, minute);
        } catch (Exception ex) {
            System.err.println("Exception: checkLogfile => ConvetStringToDate  " + ex);
            return cal.getTime();
        }
        return cal.getTime();
    }

    public static void main(String... arguments) throws IOException, Exception {
        checkLogfile lineMatcher = new checkLogfile(confs);
        lineMatcher.findBadLines("/D:/New Work/logstash/Logstash-Project/resources/log_twiter", 11, 0);
        System.out.println("Done.");
    }
}
