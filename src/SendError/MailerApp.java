/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SendError;

/**
 *
 * @author Maryam khanzadi
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import processing.DirectoryConfigs;
import processing.Logging;

public class MailerApp {

    private String mailhost = "smtp.gmail.com";
    private static String from = "zana.pardaz97@gmail.com";
    private static String password = "zana@123?";
    private static String recipients = "taregh.zana@gmail.com";

    public void Mail(String subject, String body, String Pathfile, String ContentAttachFile,
            String sender, String pass, String recipients,String Typemessage) throws Exception {
        Logging logs = new Logging();
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        sender, pass);
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setSender(new InternetAddress(sender));
        message.setSubject(subject);

        try {
            //create a temporary file
            String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(
                    Calendar.getInstance().getTime());
            String file = timeLog + ".txt";
            File logFile = new File(Pathfile + file);

            BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(ContentAttachFile);

            //Close writer
            writer.flush();
            writer.close();
            Multipart multipart = new MimeMultipart();
            message.setContent(multipart);
            DataSource source = new FileDataSource(Pathfile + file);
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(file);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            BodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(body, "text/html; charset=utf-8");
            multipart.addBodyPart(htmlPart);
//            MimeBodyPart messageBodyPart = new MimeBodyPart();
//
//            Multipart multipart = new MimeMultipart();
//
//            messageBodyPart = new MimeBodyPart();
//
//            String fileName = timeLog + ".txt";
//            DataSource source = new FileDataSource(file);
//            messageBodyPart.setDataHandler(new DataHandler(source));
//            messageBodyPart.setFileName(fileName);
//            //multipart.addBodyPart(messageBodyPart);
//
//            message.setContent(multipart,body);

        } catch (IOException e) {
            e.printStackTrace();
        }
        // message.setContent(body, "text/plain");
//        message.setText(body);
        if (recipients.indexOf(',') > 0) {
            message.setRecipients(Message.RecipientType.TO, InternetAddress
                    .parse(recipients));
        } else {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(
                    recipients));
        }

        Transport.send(message);
        System.out.println(logs.LoggingStringDate() + " [Email]  Send Successful to : " + recipients+ "For "+Typemessage);
    }

    public void EmailSend(String subject, String body, String Attachfile, String name_Core, DirectoryConfigs confs,String Typemessage,int indexcrawlerlist) throws Exception {
        MailerApp mailutils = new MailerApp();
// "List Of Websites With less than 7 news  in the last 8 hours",
//                "<br> Hi <br>  "
//                + "List Of Websites With less than 7 news  in the last 8 hours ,has been attached. <br> <br> Thanks <br> Mail sent by  Java code copied from: " + confs.From,
        mailutils.Mail(
                subject,
                body,
                confs.ReportPath + name_Core,
                Attachfile,
                confs.From,
                confs.Pass,
                confs.crawler.get(indexcrawlerlist).Recipients, Typemessage);
    }

    public static void main(String args[]) throws Exception {
        MailerApp mailutils = new MailerApp();
        mailutils.Mail(
                "Email Tests",
                "", "",
                "Mail sent by a 63 line Java code copied from: zana.pardaz97@gmail.com",
                from,
                password,
                recipients,"");
    }

}
