/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SendError;

import RunExtractErrors.ExtractError;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.json.JSONException;
import org.json.simple.parser.ParseException;
import processing.DirectoryConfigs;
import processing.Logging;

/**
 *
 * @author Maryam khanzadi
 */
public class Solr {

    private DirectoryConfigs confs;
    private static int indexcrawlerlist;

    public Solr(DirectoryConfigs configs, int indexcrawlerlist) {
        confs = configs;
        this.indexcrawlerlist = indexcrawlerlist;
    }

    public void fetchindexSolr(String UrlSolr, String name_Core) {
        Logging logs = new Logging();

        try {
            SolrQuery query = new SolrQuery();
            query.setQuery("*:*");
            query.setRows(0);
            query.setFacet(true);
            //query.addFacetField("source_id");
            query.addFacetField("source_name");

            // query.set("fl", "likes_id");
            Date now = new Date();
            String now_utc = getUTC_Date(now);
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.HOUR_OF_DAY, -8);
            Date start = cal.getTime();
            String start_utc = getUTC_Date(start);
            query.set("fq", "crawl_date:[" + start_utc + " TO " + now_utc + "]");
            HttpSolrClient server = new HttpSolrClient(UrlSolr + name_Core);
            QueryResponse response = null;
            try {
                response = server.query(query);
            } catch (SolrServerException ex) {
                Logger.getLogger(ExtractError.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (response.getResults().getNumFound() == 0) {
                return;
            }
            List<FacetField> fflist = response.getFacetFields();
            String ContentAttachFile = null;
            for (FacetField ff : fflist) {

                String ffname = ff.getName();
                int ffcount = ff.getValueCount();
                List<FacetField.Count> counts = ff.getValues();
                for (FacetField.Count c : counts) {
                    String facetLabel = c.getName();
                    long facetCount = c.getCount();
                    if (facetCount <= 7) {
                        ContentAttachFile += facetLabel + ":" + facetCount + "\n\n";

                    }
                }
            }
            if (ContentAttachFile != null) {
                SendMessage sendmsssg = new SendMessage();
                sendmsssg.sendTextMessage("List Of Websites With less than 7 news  in the last 24 hours \n"+ContentAttachFile," Check Solar => "+server.getBaseURL());
//                MailerApp mailutils = new MailerApp();
//                mailutils.EmailSend("List Of Websites With less than 7 news  in the last 24 hours",
//                        "<br> Hi <br>  "
//                        + "List Of Websites With less than 7 news  in the last 24 hours ,has been attached. <br> <br> Thanks <br> Mail sent by  Java code copied from: " + confs.From,
//                        ContentAttachFile, name_Core + "_", confs, " Error in Solr", indexcrawlerlist);
            }

        } catch (Exception ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]:  fetchindexSolr: " + ex);
            ex.printStackTrace();
        }
    }

    private String getUTC_Date(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return formatter.format(date);
    }

    public static void main(String[] args) throws JSONException, ParseException, URISyntaxException {
        DirectoryConfigs config = new DirectoryConfigs("configs.prop");

        Solr solr = new Solr(config, indexcrawlerlist);

        solr.fetchindexSolr(
                "http://185.105.239.181:8983/solr/", "arab_news");
    }
}
