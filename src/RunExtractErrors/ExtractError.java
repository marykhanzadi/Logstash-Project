/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RunExtractErrors;

import SendError.Solr;
import SendError.checkLogfile;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import processing.DirectoryConfigs;
import processing.Logging;
import processing.NewsMeta;

/**
 *
 * @author Maryam khanzadi
 */
public class ExtractError {

    private final DirectoryConfigs confs;

    public ExtractError(DirectoryConfigs configs) {
        confs = configs;
    }

    public void RunExtractTimer() throws InterruptedException {
        Timer timer = new Timer();
        
        Logging logs = new Logging();
        long interval_logfile = confs.duration_log; //1000 * 60 * 8;// //Execute Java Timer at every 8 minutes
        long interval_solr = confs.duration_solr;// 1000 * 60 * 60 * 24;////Execute Java Timer at every 24 hours

        //timer.schedule(new FetcherTimer_SolrCheack(), 0, interval_solr);
       // Thread.sleep(3 * 60 * 1000);

        int indexcrawlerlist = 0;
        for (NewsMeta entry : confs.crawler) {

            Iterator<String> itr = entry.logs_path.iterator();
            while (itr.hasNext()) {

                String namefile = itr.next();
                System.out.print(logs.LoggingStringDate() + namefile.replaceFirst("/", " [Read File]  "));
                Integer get = entry.LastLineRead.get(namefile);
                System.out.println(" From Line: " + get);
                timer.schedule(new FetcherTimer_Log(namefile, get, indexcrawlerlist), 0, interval_logfile);
                Thread.sleep(1 * 60 * 1000);

            }
            indexcrawlerlist++;
        }
    }

    class FetcherTimer_Log extends TimerTask {

        String Namefilelog;
        Integer lastlineread;
        int indexcrawlerlist;
        int indexloglist;

        public FetcherTimer_Log(String Namefilelog, Integer lastlineread, int indexcrawlerlist) {
            this.Namefilelog = Namefilelog;
            this.lastlineread = lastlineread;
            this.indexcrawlerlist = indexcrawlerlist;

        }

        @Override
        public void run() {
            cheakfilelog();

        }

        private void cheakfilelog() {
            Logging logs = new Logging();
            try {
                checkLogfile checkfile = new checkLogfile(confs);
                lastlineread = checkfile.findBadLines(Namefilelog, lastlineread, indexcrawlerlist);
                confs.crawler.get(indexcrawlerlist).LastLineRead.put(Namefilelog, lastlineread);

            } catch (Exception ex) {
                System.err.println(logs.LoggingStringDate() + " [Error]:  cheakfilelog: " + ex);
            }

        }
    }

    class FetcherTimer_SolrCheack extends TimerTask {

        private int indexcrawlerlist = 0;

        @Override
        public void run() {

            confs.crawler.forEach((entry) -> {

                entry.ServerUrls.entrySet().forEach((en) -> {
                    String Core = en.getValue();
                    String UrlSolr = en.getKey();
                    Solr solr = new Solr(confs, this.indexcrawlerlist);
                    solr.fetchindexSolr(UrlSolr, Core);

                });
                this.indexcrawlerlist++;
            });
        }

    }

    public static void main(String[] args) {

    }

}
